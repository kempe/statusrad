#include <stdint.h>
#include <dev/acpica/acpiio.h>
#include <fcntl.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <sys/sysctl.h>
#include <sys/types.h>
#include <unistd.h>

#define SLEEP_FOR 5

#define TEMP_NAME "hw.acpi.thermal.tz0.temperature"
#define FREQ_NAME "dev.cpu.0.freq"
#define BATTERY_NUMBER 0

#define SEPARATOR " | "

#define APPEND(func) printf(SEPARATOR); func

void exit_with_message(const char* msg)
{
		perror(msg);
		exit(1);
}

unsigned int sysctl_int(const char *name)
{
	unsigned int res;
	size_t size;

	size = sizeof(res);
	if (sysctlbyname(name, &res, &size, NULL, 0) == -1)
		exit_with_message("sysctl failed");

	return res;
}

void get_cpu_temp(const char *name)
{
	unsigned int temp;

	temp = sysctl_int(name);
	printf("T: %.1f °C", temp / 10.f - 273.1f);
}

void get_cpu_frequency(const char *name)
{
	unsigned int freq;

	freq = sysctl_int(name);
	printf("F: %u MHz", freq);
}

void get_volume()
{
	static int fd = -1;
	int volume;

	if (fd == -1)
	{
		fd = open("/dev/mixer", O_RDONLY);
		if (fd == -1)
			exit_with_message("opening /dev/mixer failed");
	}

	if (ioctl(fd, SOUND_MIXER_READ_VOLUME, &volume) == -1)
		exit_with_message("querying volume failed");

	printf("V: %i:%i", volume & 0x7F, (volume >> 8) & 0x7F);
}

void get_battery_info(int batnum)
{
	union acpi_battery_ioctl_arg battio;
	static int fd = -1;
	int state, hours = 0, minutes = 0, percent = 0;
	char *symbol, remaining[20];

	memset(remaining, 0, sizeof(remaining));

	if (fd == -1)
	{
		fd = open("/dev/acpi", O_RDONLY);
		if (fd == -1)
			exit_with_message("could not open /dev/acpi");
	}

	battio.unit = batnum;
	if (ioctl(fd, ACPIIO_BATT_GET_BATTINFO, &battio) == -1)
		exit_with_message("ACPIIO_BATT_GET_BATTINFO failed");

	if (battio.battinfo.state != ACPI_BATT_STAT_NOT_PRESENT)
	{
		state = (battio.battinfo.state & ACPI_BATT_STAT_BST_MASK);
		if (state == 0)
			symbol = "=";
		else if (state & ACPI_BATT_STAT_DISCHARG)
			symbol = "↓";
		else if (state & ACPI_BATT_STAT_CHARGING)
			symbol = "↑";
		else
			symbol = "?";

		if (battio.battinfo.min > 0)
		{
			hours = battio.battinfo.min / 60;
			minutes = battio.battinfo.min % 60;

			snprintf(remaining, sizeof(remaining),
			         " (%02i:%02i)", hours, minutes);
		}

		if (battio.battinfo.cap > 0)
			percent = battio.battinfo.cap;

		printf("B: %s %i %%%s", symbol, percent, remaining);
	}
}

int main()
{
	setlocale(LC_ALL, "");

	while (1)
	{
		APPEND(get_battery_info(BATTERY_NUMBER));
		APPEND(get_cpu_temp(TEMP_NAME));
		APPEND(get_cpu_frequency(FREQ_NAME));
		APPEND(get_volume());

		printf(SEPARATOR "\n");
		fflush(stdout);

		sleep(SLEEP_FOR);
	}
}
